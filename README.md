This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## To inspect bundle sizes:
1. remove the `"postbuild": "node uploadSourceMaps.js",` line from package.json as it deletes the source maps
2. Run `source-map-explorer ./build/static/js/*.js`


# To run the app locally without a local dev server
```
export REACT_APP_API_URL="https://koinly-staging.herokuapp.com"
npm start
```

OR

```ruby
REACT_APP_API_URL="https://koinly-staging.herokuapp.com" npm start
```


# Seeding data
Once you have got the website working locally, setup an account using email (no neeed for verification, so can be dummy email) and create 2 wallets:
1. Go to Wallets > Add wallet
1. Search for ETH
1. Click on it and select Auto sync
1. Enter this address and hit save: 0xcd917bf1dbae5dd0de7daae63926ec81b42faf77
1. Repeat the process to create new wallet but this time enter this address: 0x1a201469d96f7d443e71ee3997efdd6f2ed33f50
1. Wait for data to be imported.
1. You should see some data fill up on the dashboard.